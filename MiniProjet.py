import math
import random
import time
import numpy as np

infini = math.inf

# UTILITAIRES 
##
# Quicksort algo
def quickSort(alist):
   quickSortHelper(alist,0,len(alist)-1)
def quickSortHelper(alist,first,last):
   if first<last:
       splitpoint = partition(alist,first,last)
       quickSortHelper(alist,first,splitpoint-1)
       quickSortHelper(alist,splitpoint+1,last)
def partition(alist,first,last):
   pivotvalue = alist[first]
   leftmark = first+1
   rightmark = last
   done = False
   while not done:
       while leftmark <= rightmark and alist[leftmark] <= pivotvalue:
           leftmark = leftmark + 1
       while alist[rightmark] >= pivotvalue and rightmark >= leftmark:
           rightmark = rightmark -1
       if rightmark < leftmark:
           done = True
       else:
           temp = alist[leftmark]
           alist[leftmark] = alist[rightmark]
           alist[rightmark] = temp
   temp = alist[first]
   alist[first] = alist[rightmark]
   alist[rightmark] = temp


   return rightmark

#Création des listes/Système Exponentiel
#Création répartie uniformément
def CreerListe(nbBocaux,max):
    # On ne peut pas avoir des bocaux uniformément réparti si max < nbBocaux
    if(nbBocaux>max):
        print("Impossible de créer une liste uniformément répartie")
        return
    
    #Si max = nbBocaux alors les bocaux sont de taille 1,2,3,4,...,max
    if(nbBocaux==max):
        liste = []
        for i in range (1,max+1):
            liste.append(i)
        return liste
    
    list = []
    list.append(1)
    i = 2
    Absent = True
    
    #Sinon on réparti de manière uniforme entre 2 et nbBocaux
    while(i<nbBocaux+1):
        list.append(math.floor(  i * max/(nbBocaux) ))
        i+=1
    #print(list)
    return list
    

def SystemExpo(d,taille):
    #Créait un systeme exponentiel de valeur d et de taille taille
    list = []
    for i in range (0,taille):
        list.append(d**i)
    return list

##
##



def RechercheExhaustive(k,V,s):
    
    if(s<0):
        return infini
    else:
        if(s==0):
            return 0
        else:
            nbCont = s
            for i in range(k):
                x = RechercheExhaustive(k,V,s-V[i])
                if(x+1 < nbCont):
                    nbCont=x+1
            return nbCont
            
##


def AlgoProgDyn(k,V,S):
    global stock
    
    stock = np.dot((-1),np.ones((S+1,k+1)))

    return ProcessDyn(k,V,S)
    
    
def ProcessDyn(k,V,S):
    global stock
    
    
    if(S == 0):
        return 0;
    if(S == 1):
        return S
    
    k = k+1
    S = S+1
    
    
    #Initialisation des lignes principales
    for i in range(0,S):
        stock[i][0] = infini
    
    for i in range(2,S):
       stock[i][1] = i
       
    for i in range(0,k):
       stock[0][i] = 0
       
    for i in range(1,k):
       stock[1][i] = 1
    
    
    #Remplissagne par ligne
    for i in range (2,S):
        for j in range(2,k):
            
            
            m1 = stock[i][j-1]
            if(i - V[j-1] < 0):
                m2 = infini
            else:
                m2 = stock[i - V[j-1]][j] + 1
            #print(m1,m2)
            
            stock[i][j] = min(m1,m2)
    
    #print(stock,"\n")
    
    #print(stock[S-1][k-1])
    return stock[S-1][k-1]
    
##

def AlgoGlouton(k,V,S):
    cpt = 0
    i = k-1
    while(i>=0):
        while(V[i] <= S):
            S = S - V[i]
            cpt+=1
        i-=1
    return cpt
    
##
    
def TestGloutonCompatible(k,V):
    if(k<3):
        return True
    elif(k>=3):
        for S in range (V[3] + 2, V[k-2]+V[k-1]-1):
            for j in range (k):
                if((V[j-1]<S) & (AlgoGlouton(k,V,S) > 1+AlgoGlouton(k,V,S-V[j-1]))):
                    return False
    return True



def PourcentageAlgoGloutonCompatible(nbTest,tailleTab,Inc):
    """
    Inc va incrémenter le max en conservant une liste uniformément réparti exemple :
    itération i     : [1, 4, 6, 8, 10, 12, 14, 16, 18, 20]
    itération i+1   : [1, 4, 6, 8, 11, 13, 15, 17, 19, 22]
    
    
    Merci de bien vouloir remplir ces conditions :
    Inc         est un entier
    tailleTab   est un entier
    nbTest      est un entier
    
    Inc         > 0
    tailleTab   > 1
    
    
    Exemple : 
    nbTest = 100
    tailleTab = 10
    Inc = 2
    
    """
    
    if(nbTest <= 0):
        return
    if(tailleTab < 0):
        return
    if(tailleTab <= 2):
        return 100
    if(Inc < 1):
        return
    
    cpt = 0
    
    
    
    print("\nNombre de tests : ",nbTest)
    print("Taille du tableau : ",tailleTab)
    print("Valeur incrémentée de V[i] : ",Inc)
    for i in range (math.floor(tailleTab / Inc),nbTest+math.floor(tailleTab / Inc)):
        max = Inc * i
        liste = CreerListe(tailleTab,max)
        quickSort(liste)
        #print("Test de : ",liste)
            
        if(TestGloutonCompatible(tailleTab,liste)):
            cpt+=1
    res = cpt / (nbTest * 1.0) * 100
    print("\nLe pourcentage d'algo glouton compatible est de : ",res,"%")
    return res


def StatistiqueGlouton(nbTest,tailleTab,Inc):
    """
    Inc va incrémenter le max en conservant une liste uniformément réparti exemple :
    itération i     : [1, 4, 6, 8, 10, 12, 14, 16, 18, 20]
    itération i+1   : [1, 4, 6, 8, 11, 13, 15, 17, 19, 22]
    
    
    Merci de bien vouloir remplir ces conditions :
    Inc         est un entier
    tailleTab   est un entier
    nbTest      est un entier
    
    Inc         > 0
    tailleTab   > 1
    
    
    Exemple : 
    nbTest = 100
    tailleTab = 10
    Inc = 2
    """
    
    
    
    
    if(nbTest <= 0):
        return
    if(tailleTab < 0):
        return
    if(Inc < 1):
        return
    
    
    EcartMaxG = 0
    EcartMinG = infini
    EcartMoyenG = 0
    
    cpt = 0
    Pourcentage = 0
    
    
    
    print("\nNombre de tests : ",nbTest)
    print("Taille du tableau : ",tailleTab)
    print("Valeur incrémentée de V[i] : ",Inc)
    print("Valeurs de S tirés aléatoirement entre 1 et 500")
    print()
    for i in range (math.floor(tailleTab / Inc) + 1,nbTest+math.floor(tailleTab / Inc) + 1):
        #Initialisation
        max = Inc * i
        S = random.randint(0, 500)
        liste = CreerListe(tailleTab,max)
        quickSort(liste)
        #print("Test de : ",liste,"\t S = ",S)
            
        #Si glouton compatible alors on incrémente le cpt
        if(TestGloutonCompatible(tailleTab,liste)):
            cpt+=1
        else:
        #Sinon on calcul l'écart min etc etc.
            resG = AlgoGlouton(tailleTab,liste,S)
            resD = AlgoProgDyn(tailleTab,liste,S)
            
            Ecart = resG - resD
            #print("Ecart entre AlgoGlouton et AlgoProgDyn : ",Ecart)
            if(Ecart > EcartMaxG):
                EcartMaxG = Ecart
            if(Ecart < EcartMinG):
                EcartMinG = Ecart
                
            EcartMoyenG += Ecart
                
                
                
    Pourcentage = (cpt / (nbTest * 1.0)) * 100
    EcartMoyenG = EcartMoyenG / nbTest
    
    print("\nLe pourcentage d'algo glouton compatible est de : ",Pourcentage,"%")
    print("EcartMin = ",EcartMinG)
    print("EcartMax = ",EcartMaxG)
    print("EcartMoyen = ",EcartMoyenG)
    return Pourcentage
    
###
#Les ENTREES SORTIES
def lecteurFichier(f):
    """
    Le fichier doit etre présenté sous la forme suivante :
        V
        S
    """
    fichier = open(f,"r")
    global S
    global V
    
    #Initialiser S
    texte = fichier.readline()
    S = int(texte)
    
    #Initialiser le tableau
    texte = fichier.readline()
    split = texte.split(" ")
    V = []
    
    #Conversion en int et ajout dans la liste
    for val in split:
        V.append(int(val))
    if(V[0] != 1):
        print("Pas de bocal de taille 1")
    
    fichier.close()

def txtAlgoExhaustif_sur_S():
    fichier = open("AlgoExhaustif_Sur_S.txt","w")
    
    
    
    for i in range (30,76):
        k = 10
        V = SystemExpo(10,k)
        S = i
            
        start_time = time.time()
        res = RechercheExhaustive(k,V,S)
        temps = time.time() - start_time
        print("\nResultat trouvé avec la recherche exhaustive sur k = ",k," S =",S, ": ",res)
        print("Temps d'execution en seconde : ",time.time() - start_time)
        fichier.write(str(S) + " ")
        fichier.write(str(temps) + "\n")
    
    fichier.close()

def txtAlgoExhaustif_sur_k():
    fichier = open("AlgoExhaustif_Sur_k.txt","w")
    
    
    
    for i in range (1,100):
        k = i
        V = SystemExpo(10,k)
        S = 50
            
        start_time = time.time()
        res = RechercheExhaustive(k,V,S)
        temps = time.time() - start_time
        print("\nResultat trouvé avec la recherche exhaustive sur k = ",k," S =",S, ": ",res)
        print("Temps d'execution en seconde : ",time.time() - start_time)
        fichier.write(str(S) + " ")
        fichier.write(str(temps) + "\n")
    
    fichier.close()
        
def txtAlgoProgDyn_sur_S():
    fichier = open("AlgoProgDyn_Sur_S.txt","w")
    
    
    
    for i in range (50000,1000001,50000):
        k = 10
        V = SystemExpo(10,k)
        S = i
            
        start_time = time.time()
        res = AlgoProgDyn(k,V,S)
        temps = time.time() - start_time
        print("\nResultat trouvé avec la recherche dynamique sur k = ",k," S =",S, ": ",res)
        print("Temps d'execution en seconde : ",time.time() - start_time)
        fichier.write(str(S) + " ")
        fichier.write(str(temps) + "\n")
        
def txtAlgoProgDyn_sur_k():
    fichier = open("AlgoProgDyn_Sur_k.txt","w")
    
    
    
    for i in range (1,50):
        k = i
        V = SystemExpo(10,k)
        S = 100000
            
        start_time = time.time()
        res = AlgoProgDyn(k,V,S)
        temps = time.time() - start_time
        print("\nResultat trouvé avec la recherche dynamique  sur k = ",k," S =",S, ": ",res)
        print("Temps d'execution en seconde : ",time.time() - start_time)
        fichier.write(str(S) + " ")
        fichier.write(str(temps) + "\n")
    
    fichier.close()
    
    
def txtAlgoGlouton_sur_S():
    fichier = open("AlgoGlouton_Sur_S.txt","w")
    
    
    
    for i in range (5000000,1000000001,5000000):
        k = 10
        V = SystemExpo(2,k)
        S = i
        
        start_time = time.time()
        res = AlgoGlouton(k,V,S)
        temps = time.time() - start_time
        print("\nResultat trouvé avec la recherche dynamique  sur k = ",k," S =",S, ": ",res)
        print("Temps d'execution en seconde : ",time.time() - start_time)
        fichier.write(str(S) + " ")
        fichier.write(str(temps) + "\n")
    
    fichier.close()
    
#Nous vous deconseillons d'utiliser cette fonction.
#Nous créons des systèmes exponentiel c'est à dire des nombres tels que 2^100000 qui occupent beaucoup de mémoire et qui rallonge les temps de comparaisons et de calculs. C'est pourquoi lorsqu'on execute cette fonction, il arrive que les temps affichés diffère des temps réels.
def txtAlgoGlouton_sur_k():
    fichier = open("AlgoGlouton_Sur_k.txt","w")
    for i in range (10000,100000,10000):
        k = i
        V = SystemExpo(2,k)
        S = 100
            
        start_time = time.time()
        res = AlgoGlouton(k,V,S)
        temps = time.time() - start_time
        print("\nResultat trouvé avec la recherche dynamique  sur k = ",k," S =",S, ": ",res)
        print("Temps d'execution en seconde : ",time.time() - start_time)
        fichier.write(str(S) + " ")
        fichier.write(str(temps) + "\n")
    
## 
##
## 










#SystemExpo(valeur d, taille de la liste)
V = SystemExpo(2,6)
k = len(V)
S = 25

print("Liste de bocaux : ",V)
print("S = ",S)


start_time = time.time()
res = RechercheExhaustive(k,V,S)
print("\nResultat trouvé avec la recherche exhaustive: ",res)
print("Temps d'execution en seconde : ",time.time() - start_time)

start_time = time.time()
res = AlgoProgDyn(k,V,S) 
print("\nResultat trouvé avec l'algorithme dynamique: ",res)
print("Temps d'execution en seconde : ",time.time() - start_time)

start_time = time.time()
res = AlgoGlouton(k,V,S) 
print("\nResultat trouvé avec l'algorithme glouton: ",res)
print("Temps d'execution en seconde : ",time.time() - start_time)

start_time = time.time()
res = TestGloutonCompatible(k,V)
print("\nLe jeu de test est glouton compatible : ",res)
print("Temps d'execution en seconde : ",time.time() - start_time)



#Générer les txt pour les données
#Le TXT a été généré sur un I7-7700k. Réajuster les valeurs dans les boucles si le temps de calcul est trop long
#txtAlgoExhaustif_sur_S()
#txtAlgoExhaustif_sur_k()
#txtAlgoProgDyn_sur_S()
#txtAlgoProgDyn_sur_k()
#txtAlgoGlouton_sur_S()


#Déconseillé voir dans la fonction pour les détails
#txtAlgoGlouton_sur_k();



#PourcentageAlgoGloutonCompatible(nombres test, taille de la liste, incrémentation)
#PourcentageAlgoGloutonCompatible(1000,10,2)

#StatistiqueGlouton(nombres test, taille de la liste, incrémentation)
StatistiqueGlouton(250,10,4)

